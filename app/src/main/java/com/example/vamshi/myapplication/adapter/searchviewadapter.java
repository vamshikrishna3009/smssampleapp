package com.example.vamshi.myapplication.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vamshi.myapplication.R;
import com.example.vamshi.myapplication.model.SearchResults;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by vamshi on 25-06-2016.
 */
public class searchviewadapter extends RecyclerView.Adapter<searchviewadapter.CustomViewHolder> {

    Context mContext;
    List<SearchResults> searchResultses;

    public searchviewadapter(Context context, List<SearchResults> data) {
        this.mContext = context;
        this.searchResultses = data;
        notifyDataSetChanged();
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_searchresults, parent, false);
        return new CustomViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        SearchResults searchResults = searchResultses.get(position);
        holder.mMssgFrom.setText(searchResults.getSender());
//        holder.mMssgCount.setText(smsdetail.getCount());
        holder.mMssgBody.setText(searchResults.getMssg());
//        if(isDateToday(Long.parseLong(smsdetail.getTime()))){
//
//            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
//            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
//            Date date = null;
//            try {
//                date = sdf.parse(getDate(Long.parseLong(smsdetail.getTime())));
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            String dateStr = sdf.format(date);
//
//
////            if(String.valueOf(hours).length()== 1 || String.valueOf(minute).length()== 1 ){
////                if(String.valueOf(hours).length()== 1) {
////                    holder.mMssgTime.setText("" + "0"+hours + ":" + minute);
////                }else{
////                    holder.mMssgTime.setText("" + +hours + ":"+"0"+ minute);
////                }
////            }else{
//                holder.mMssgTime.setText(""+dateStr.substring(11,16));
////            }
//        }else{
//            Timestamp timestamp = new Timestamp(Long.parseLong(smsdetail.getTime()));
//            Date date = new Date(timestamp.getTime());
//
//            // S is the millisecond
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM");
//            holder.mMssgTime.setText(""+simpleDateFormat.format(timestamp));
//        }

    }

    @Override
    public int getItemCount() {
        if (searchResultses!=null)
            return searchResultses.size();
        else
            return 0;
    }

//    private String getDate(long time) {
//        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
//        cal.setTimeInMillis(time);
//        String date = DateFormat.format("dd/MM/yyyy hh:mm:ss", cal).toString();
//        return date;
//    }
//
//    public static boolean isDateToday(long milliSeconds) {
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTimeInMillis(milliSeconds);
////        calendar.setTimeZone(TimeZone.getTimeZone("GMT"));
//
//        Date getDate = calendar.getTime();
//
//        calendar.setTimeInMillis(System.currentTimeMillis());
//        calendar.set(Calendar.HOUR_OF_DAY, 0);
//        calendar.set(Calendar.MINUTE, 0);
//        calendar.set(Calendar.SECOND, 0);
//
//        Date startDate = calendar.getTime();
//
//        return getDate.compareTo(startDate) > 0;
//
//    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.mssgFrom)
        TextView mMssgFrom;

//        @Bind(R.id.mssgCount)
//        TextView mMssgCount;

        @Bind(R.id.mssgBody)
        TextView mMssgBody;

//        @Bind(R.id.mssgTime)
//        TextView mMssgTime;

        public CustomViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
