package com.example.vamshi.myapplication.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vamshi.myapplication.R;
import com.example.vamshi.myapplication.model.threadMssging;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by vamshi on 25-06-2016.
 */
public class threadmssgingviewadapter extends RecyclerView.Adapter<threadmssgingviewadapter.CustomViewHolder> {

    Context mContext;
    List<threadMssging> threadMssgings;

    public threadmssgingviewadapter(Context context, List<threadMssging> data) {
        this.mContext = context;
        this.threadMssgings = data;
        notifyDataSetChanged();
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_threadmssging, parent, false);
        return new CustomViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        threadMssging threadMssging = threadMssgings.get(position);

        if(threadMssging.getSender_mssg() == null) {
            holder.mMssgFrom.setText(threadMssging.getRecipient_id());
            holder.mMssgBody.setText(threadMssging.getRecipient_mssg());

        if(isDateToday(Long.parseLong(threadMssging.getRecipient_time()))){

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                Date date = null;
                try {
                    date = sdf.parse(getDate(Long.parseLong(threadMssging.getRecipient_time())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String dateStr = sdf.format(date);
                holder.mMssgTime.setText(""+dateStr.substring(11,16));
            }else{
                Timestamp timestamp = new Timestamp(Long.parseLong(threadMssging.getRecipient_time()));
//                Date date = new Date(timestamp.getTime());
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM");
                holder.mMssgTime.setText(""+simpleDateFormat.format(timestamp));
            }
        }else if(threadMssging.getRecipient_mssg() == null) {
            holder.mMssgFrom.setText(threadMssging.getSender_id());
            holder.mMssgBody.setText(threadMssging.getSender_mssg());

        if(isDateToday(Long.parseLong(threadMssging.getRecipient_time()))){

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                Date date = null;
                try {
                    date = sdf.parse(getDate(Long.parseLong(threadMssging.getSender_time())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String dateStr = sdf.format(date);
                holder.mMssgTime.setText(""+dateStr.substring(11,16));
            }else{
                Timestamp timestamp = new Timestamp(Long.parseLong(threadMssging.getSender_time()));
//                Date date = new Date(timestamp.getTime());
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM");
                holder.mMssgTime.setText(""+simpleDateFormat.format(timestamp));
            }
        }
    }

    @Override
    public int getItemCount() {
        if (threadMssgings!=null)
            return threadMssgings.size();
        else
            return 0;
    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("dd/MM/yyyy hh:mm:ss", cal).toString();
        return date;
    }

    public static boolean isDateToday(long milliSeconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
//        calendar.setTimeZone(TimeZone.getTimeZone("GMT"));

        Date getDate = calendar.getTime();

        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        Date startDate = calendar.getTime();

        return getDate.compareTo(startDate) > 0;

    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.mssgFrom)
        TextView mMssgFrom;

//        @Bind(R.id.mssgCount)
//        TextView mMssgCount;

        @Bind(R.id.mssgBody)
        TextView mMssgBody;

        @Bind(R.id.mssgTime)
        TextView mMssgTime;

        public CustomViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
