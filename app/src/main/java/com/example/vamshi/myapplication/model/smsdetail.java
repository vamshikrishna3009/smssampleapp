package com.example.vamshi.myapplication.model;

import java.util.ArrayList;

/**
 * Created by vamshi on 25-06-2016.
 */

public class smsdetail {

    String id;
    int icon;
    String sender;
    ArrayList<String> msg = new ArrayList<>();
//    String msg;
    String time;
    String count;

    public smsdetail() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

//    public String getMsg() {
//        return msg;
//    }
    public ArrayList<String> getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg.add(msg);
//        this.msg=(msg);
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}