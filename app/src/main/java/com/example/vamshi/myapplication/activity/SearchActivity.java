package com.example.vamshi.myapplication.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.example.vamshi.myapplication.R;
import com.example.vamshi.myapplication.adapter.searchviewadapter;
import com.example.vamshi.myapplication.model.SearchResults;
import com.example.vamshi.myapplication.storage.MessagesDB;
import com.example.vamshi.myapplication.utils.RecyclerItemClickListener;
import com.example.vamshi.myapplication.utils.SimpleDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SearchActivity extends AppCompatActivity {

    @Bind(R.id.searchResultsList)
    RecyclerView mSearchResults;

    @Bind(R.id.searchTitle)
    TextView mSearchTitle;

    List<SearchResults> searchResultses = new ArrayList<>();

    MessagesDB mydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        mydb = MessagesDB.getInstance(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSearchResults.setLayoutManager(new LinearLayoutManager(this));

        String searchQuery = getIntent().getStringExtra("SearchQuery");
        mSearchTitle.setText(searchQuery);
        Cursor cur = mydb.getSearchResults(searchQuery);

        while (cur.moveToNext()){
            SearchResults searchResults = new SearchResults();
            searchResults.setSender(cur.getString(1));
            searchResults.setMssg(cur.getString(3));
            searchResultses.add(searchResults);
        }

        searchviewadapter listAdapter = new searchviewadapter(this, searchResultses);

        mSearchResults.addItemDecoration(new SimpleDividerItemDecoration(this));

        mSearchResults.setAdapter(listAdapter);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSearchResults.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent inThreadActivity = new Intent(SearchActivity.this, InThreadActivity.class);
                        inThreadActivity.putExtra("ContactNumber", searchResultses.get(position).getSender());
                        inThreadActivity.putExtra("SenderContactNumber", "me");
                        startActivity(inThreadActivity);
                        finish();
                    }
                })
        );
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
