package com.example.vamshi.myapplication.model;

import java.util.List;

import lombok.Data;

/**
 * Created by vamshi on 25-06-2016.
 */
@Data
public class smsdetaillist {

    List<smsdetail> data;

}
