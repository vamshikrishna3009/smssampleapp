package com.example.vamshi.myapplication.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by vamshi on 28-06-2016.
 */
public class MessagesDB extends SQLiteOpenHelper {

    private static MessagesDB mInstance = null;

    public static final String DATABASE_NAME = "MessagesDB.db";

    /**
     *  Message Starts here
     **/
    public static final String MESSAGE_DB_TABLE_NAME = "Messages";
    public static final String Message_DB_COL_1 = "ID";
    public static final String Message_DB_COL_2 = "RECIPIENT_ID";
    public static final String Message_DB_COL_3 = "SENDER_ID";
    public static final String Message_DB_COL_4 = "RECIPIENT_MSSG";
    public static final String Message_DB_COL_5 = "SENDER_MSSG";
    public static final String Message_DB_COL_6 = "RECIPIENT_TIME";
    public static final String Message_DB_COL_7 = "SENDER_TIME";
    /**Ends here**/

    public static MessagesDB getInstance(Context ctx) {
        if (mInstance == null) {
            mInstance = new MessagesDB(ctx.getApplicationContext());
        }
        return mInstance;
    }

    private MessagesDB(Context context) {
        super(context, DATABASE_NAME, null, 1);
        //SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_MESSAGES_TABLE = "CREATE TABLE " + MESSAGE_DB_TABLE_NAME + "(" + Message_DB_COL_1 + " INTEGER PRIMARY KEY AUTOINCREMENT," + Message_DB_COL_2 +
                " TEXT," + Message_DB_COL_3 + " TEXT," + Message_DB_COL_4 + " TEXT," + Message_DB_COL_5 + " TEXT, " + Message_DB_COL_6 +
                " TEXT," + Message_DB_COL_7 + " TEXT);";

        try {
            db.beginTransaction();
            db.execSQL(CREATE_MESSAGES_TABLE);
            db.execSQL("CREATE VIRTUAL TABLE fts_table USING fts3 "+ "(" + Message_DB_COL_1 + " INTEGER PRIMARY KEY AUTOINCREMENT," + Message_DB_COL_2 +
                    " TEXT," + Message_DB_COL_3 + " TEXT," + Message_DB_COL_4 + " TEXT," + Message_DB_COL_5 + " TEXT, " + Message_DB_COL_6 +
                    " TEXT," + Message_DB_COL_7 + " TEXT);");
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Database Create", "Database Creation is Not Successful");
        } finally {
            db.endTransaction();
//            db.close();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        try {
            db.beginTransaction();
            db.execSQL("DROP TABLE IF EXISTS " + MESSAGE_DB_TABLE_NAME);
            this.onCreate(db);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Data Upgrade", "Database Upgrade is Not Successful");
        } finally {
            db.endTransaction();
//            db.close();
        }

    }

    public boolean insertMessage(String recipient_id, String sender_id, String recipient_mssg, String sender_mssg, String recipient_time, String sender_time) {

        SQLiteDatabase db = this.getWritableDatabase();
        long result = 0;
        try {
            db.beginTransaction();
            ContentValues contentValues = new ContentValues();
            contentValues.put(Message_DB_COL_2, recipient_id);
            contentValues.put(Message_DB_COL_3, sender_id);
            contentValues.put(Message_DB_COL_4, recipient_mssg);
            contentValues.put(Message_DB_COL_5, sender_mssg);
            contentValues.put(Message_DB_COL_6, recipient_time);
            contentValues.put(Message_DB_COL_7, sender_time);

            if(checkIfExists(recipient_id,recipient_mssg,recipient_time).getCount()==0){
                db.insertWithOnConflict("fts_table",Message_DB_COL_6,contentValues,SQLiteDatabase.CONFLICT_REPLACE);
                result = db.insertWithOnConflict(MESSAGE_DB_TABLE_NAME, Message_DB_COL_6, contentValues,SQLiteDatabase.CONFLICT_REPLACE);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Message", "Inserting in the Message is Unsuccessful");
        } finally {
            db.endTransaction();
//            db.close();
        }

        if (result == -1)
            return false;
        else
            return true;
    }

    private Cursor checkIfExists(String recipient_id,String recipient_mssg,String recipient_time) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res1 = null;
        try {
            db.beginTransaction();
//            res1 = db.rawQuery("SELECT * FROM " + MESSAGE_DB_TABLE_NAME , null);
            res1 = db.rawQuery("SELECT * FROM " + MESSAGE_DB_TABLE_NAME + " WHERE " + Message_DB_COL_2 + " = ? AND "+Message_DB_COL_4+" = ?", new String[]{recipient_id,recipient_mssg});
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Database Messages", "Get Messages is Unsuccessful");
        } finally {
//            if(res1 != null) res1.close();
            db.endTransaction();

//            db.close();
        }
        return res1;
    }

    public Cursor getSenderRecipientMessages(String recipient_Id,String sender_Id){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res1 = null;
        try {
            db.beginTransaction();
//            res1 = db.rawQuery("SELECT * FROM " + MESSAGE_DB_TABLE_NAME , null);
            res1 = db.rawQuery("SELECT * FROM " + MESSAGE_DB_TABLE_NAME + " WHERE " + Message_DB_COL_2 + " = ? AND "+Message_DB_COL_3+" = ?", new String[]{recipient_Id,sender_Id});
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Database Messages", "Get Messages is Unsuccessful");
        } finally {
//            if(res1 != null) res1.close();
            db.endTransaction();
//            db.close();
        }

        return res1;
    }

    public Cursor getSearchResults(String searchQuery) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res1 = null;
        try {
            db.beginTransaction();
//            res1 = db.rawQuery("SELECT * FROM " + MESSAGE_DB_TABLE_NAME, null);
            res1 = db.rawQuery("SELECT * FROM fts_table WHERE  fts_table MATCH ?", new String[]{searchQuery});
            //            res1 = db.rawQuery("SELECT * FROM " + MESSAGE_DB_TABLE_NAME + " WHERE " + Vehicle_Rc_data_COL_3 + "= ?", new String[]{columnName});
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Database Messages", "Get Messages is Unsuccessful");
        } finally {
            //            if(res1 != null) res1.close();
            db.endTransaction();
            //            db.close();
        }

        return res1;
    }

    public Cursor getDistinctMessage(){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res1 = null;
        try {
            db.beginTransaction();
//            res1 = db.rawQuery("SELECT * FROM " + MESSAGE_DB_TABLE_NAME + "TB1 WHERE (SELECT MAX(RECIPIENT_TIME) FROM Messages GROUP BY RECIPIENT_ID )"+" GROUP BY "+Message_DB_COL_2 +" ORDER BY "+Message_DB_COL_6+" DESC", null);

//            res1 = db.rawQuery("SELECT * FROM " + MESSAGE_DB_TABLE_NAME + " WHERE ID IN (\n" +
//                    "    SELECT MAX(ID)\n" +
//                    "    FROM Messages\n" +
//                    "    GROUP BY RECIPIENT_ID\n" +
//                    " ORDER BY RECIPIENT_TIME DESC"+
//                    ");", null);
            res1 = db.rawQuery("SELECT ID,RECIPIENT_ID,SENDER_ID,RECIPIENT_MSSG,SENDER_MSSG,RECIPIENT_TIME,SENDER_TIME FROM (  SELECT ID,RECIPIENT_ID,SENDER_ID,RECIPIENT_MSSG,SENDER_MSSG,RECIPIENT_TIME,SENDER_TIME\n" +
                    "        FROM Messages\n" +
                    "        ORDER BY RECIPIENT_TIME ASC) GROUP BY RECIPIENT_ID ORDER BY RECIPIENT_TIME DESC ", null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Database Messages", "Get Messages is Unsuccessful");
        } finally {
//            if(res1 != null) res1.close();
            db.endTransaction();
//            db.close();
        }

        return res1;
    }

    public boolean updateEditData(String recipient_id, String sender_id, String recipient_mssg, String sender_mssg, String recipient_time, String sender_time) {

        SQLiteDatabase db = this.getWritableDatabase();
        long result = 0;
        try {
            db.beginTransaction();
            ContentValues contentValues = new ContentValues();
            contentValues.put(Message_DB_COL_2, recipient_id);
            contentValues.put(Message_DB_COL_3, sender_id);
            contentValues.put(Message_DB_COL_4, recipient_mssg);
            contentValues.put(Message_DB_COL_5, sender_mssg);
            contentValues.put(Message_DB_COL_6, recipient_time);
            contentValues.put(Message_DB_COL_7, sender_time);
            result = db.insert(MESSAGE_DB_TABLE_NAME, null, contentValues);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Message", "Inserting in the Message is Unsuccessful");
        } finally {
            db.endTransaction();
//            db.close();
        }

        if (result == 1)
            return true;
        else
            return false;
    }

    public void deleteAllMssgs() {

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            db.delete(MESSAGE_DB_TABLE_NAME, null,null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Message Data", "Delete Messages is Unsuccessful");
        } finally {
            db.endTransaction();
//            db.close();
        }

    }

}
