package com.example.vamshi.myapplication.activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.vamshi.myapplication.R;
import com.example.vamshi.myapplication.storage.MessagesDB;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NewMssgActivity extends AppCompatActivity {

    @Bind(R.id.sendMssg)
    EditText mSendMssg;

    @Bind(R.id.MssgContent)
    EditText mMssgContent;

    @Bind(R.id.send)
    ImageView mSend;

    MessagesDB mydb;

    Activity mActivity;

    String SMS_SENT = "SMS_SENT";
    String SMS_DELIVERED = "SMS_DELIVERED";

    PendingIntent sentPendingIntent,deliveredPendingIntent;
    BroadcastReceiver smsSentReceiver, smsDeliveredReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_mssg);
        ButterKnife.bind(this);
        mActivity = this;
        mydb = MessagesDB.getInstance(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sentPendingIntent = PendingIntent.getBroadcast(mActivity, 0, new Intent(SMS_SENT), 0);
        deliveredPendingIntent = PendingIntent.getBroadcast(mActivity, 0, new Intent(SMS_DELIVERED), 0);
        
        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(mSendMssg.getText().toString().length() == 9 && mMssgContent.getText().toString().length()>=3){
                    smssender(mSendMssg.getText().toString(),mMssgContent.getText().toString());
//                }else {
//                    Toast.makeText(NewMssgActivity.this, "Please fill the details", Toast.LENGTH_SHORT).show();
//                }
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }

    @Override
    protected void onResume() {
        super.onResume();

        smsSentReceiver = new BroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(context, "SMS sent successfully", Toast.LENGTH_SHORT).show();
                        mydb.insertMessage(mSendMssg.getText().toString(),"me",mMssgContent.getText().toString(),null,null,String.valueOf(System.currentTimeMillis()));
                        onBackPressed();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(context, "Generic failure cause", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(context, "Service is currently unavailable", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(context, "No pdu provided", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(context, "Radio was explicitly turned off", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };

        smsDeliveredReceiver = new BroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(NewMssgActivity.this, "SMS delivered", Toast.LENGTH_SHORT).show();

                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(NewMssgActivity.this, "SMS not delivered", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };

        registerReceiver(smsSentReceiver, new IntentFilter(SMS_SENT));
        registerReceiver(smsDeliveredReceiver, new IntentFilter(SMS_DELIVERED));
    }

    public void smssender(String phoneNumber, String smsBody) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(phoneNumber, null, smsBody, sentPendingIntent, deliveredPendingIntent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(smsSentReceiver);
        unregisterReceiver(smsDeliveredReceiver);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
