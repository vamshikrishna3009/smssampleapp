package com.example.vamshi.myapplication.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vamshi.myapplication.R;
import com.example.vamshi.myapplication.adapter.smsviewadapter;
import com.example.vamshi.myapplication.model.smsdetail;
import com.example.vamshi.myapplication.storage.MessagesDB;
import com.example.vamshi.myapplication.utils.RecyclerItemClickListener;
import com.example.vamshi.myapplication.utils.SimpleDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity{

    private static final String TAG = "MainActivity";

    @Bind(R.id.newmssg)
    ImageView mNewmssg;
    @Bind(R.id.sms_mssg_list)
    RecyclerView mSmsList;
    @Bind(R.id.searchQuery)
    EditText mSearchQuery;
    @Bind(R.id.search)
    ImageView mSearchButton;
    @Bind(R.id.searchclose)
    ImageView mSearchClose;
    @Bind(R.id.activityname)
    TextView mactivityName;
    Context mContext;
    List<smsdetail> smsList = new ArrayList<smsdetail>();
    MessagesDB mydb;
    smsviewadapter listAdapter;
    private boolean mResolvingError = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mContext = this;

        mydb = MessagesDB.getInstance(this);

        mNewmssg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NewMssgActivity.class));
            }
        });

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSearchQuery.getVisibility() == View.VISIBLE) {
                    if (mSearchQuery.getText().toString().isEmpty()) {
                        Toast.makeText(MainActivity.this, "Please Enter something to search", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent searchActivity = new Intent(MainActivity.this, SearchActivity.class);
                        searchActivity.putExtra("SearchQuery", mSearchQuery.getText().toString());
                        startActivity(searchActivity);
                    }

                } else {
                    mactivityName.setVisibility(View.GONE);
                    mSearchQuery.setVisibility(View.VISIBLE);
                    mSearchButton.setVisibility(View.VISIBLE);
                    mSearchClose.setVisibility(View.VISIBLE);
                }
//                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//                getSupportActionBar().setDisplayShowHomeEnabled(true);

            }
        });

        mSearchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSearchQuery.getVisibility() == View.VISIBLE) {
                    mactivityName.setVisibility(View.VISIBLE);
                    mSearchQuery.setVisibility(View.GONE);
                    mSearchButton.setVisibility(View.VISIBLE);
                    mSearchClose.setVisibility(View.GONE);
//                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                }
            }
        });


        mSmsList.setLayoutManager(new LinearLayoutManager(this));
        mSmsList.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent inThreadActivity = new Intent(MainActivity.this, InThreadActivity.class);
                        inThreadActivity.putExtra("ContactNumber", smsList.get(position).getSender());
                        inThreadActivity.putExtra("SenderContactNumber", "me");
                        startActivity(inThreadActivity);
                    }
                })
        );
    }

    private void fetchSMSData() {

        Uri uri = Uri.parse("content://sms/inbox");
        Cursor c = getContentResolver().query(uri, null, null, null, "date DESC");
        Log.d("1", "fetchSMSData: ");
        // Read the sms data and store it in the list
        while (c.moveToNext()) {

            boolean inserted = mydb.insertMessage(c.getString(c.getColumnIndexOrThrow("address")).toString(), "me", c.getString(c.getColumnIndexOrThrow("body")).toString(), null, c.getString(c.getColumnIndexOrThrow("date")).toString(), null);

            if (inserted)
                Log.d("INSERTED", "fetchSMSData: YES");

        }
//        c.close();

        Cursor c1 = mydb.getDistinctMessage();

        while (c1.moveToNext()) {
            smsdetail sms = new smsdetail();
            Log.d("DATA", "fetchSMSData: " + c1.getString(1));
            Log.d("DATA", "fetchSMSData: " + c1.getString(3));
            sms.setSender(c1.getString(1));
            sms.setTime(c1.getString(5));
            sms.setMsg(c1.getString(3));
            smsList.add(sms);
        }
//        c1.close();
        listAdapter = new smsviewadapter(this, smsList);

        mSmsList.addItemDecoration(new SimpleDividerItemDecoration(mContext));

        mSmsList.setAdapter(listAdapter);

    }
    @Override
    protected void onResume() {
        super.onResume();
        listAdapter = null;
        fetchSMSData();
    }
}
