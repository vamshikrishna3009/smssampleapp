package com.example.vamshi.myapplication.activity;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.example.vamshi.myapplication.R;
import com.example.vamshi.myapplication.adapter.threadmssgingviewadapter;
import com.example.vamshi.myapplication.model.threadMssging;
import com.example.vamshi.myapplication.storage.MessagesDB;
import com.example.vamshi.myapplication.utils.SimpleDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class InThreadActivity extends AppCompatActivity {

    @Bind(R.id.contactNumber)
    TextView mContactNumber;

//    @Bind(R.id.searchString)
//    TextView mSearchString;
//
//    @Bind(R.id.searchButton)
//    ImageView mSearchButton;

    @Bind(R.id.threadMssging)
    RecyclerView mThreadMssging;

    MessagesDB mydb;

    List<threadMssging> threadMssgings = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_thread);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mThreadMssging.setLayoutManager(new LinearLayoutManager(this));

        mydb = MessagesDB.getInstance(this);

        String contactNumber = getIntent().getStringExtra("ContactNumber");
        String sendercontactNumber = getIntent().getStringExtra("SenderContactNumber");
        mContactNumber.setText(contactNumber);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        mSearchButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Cursor cur = mydb.getSearchResults(mSearchString.getText().toString());
//                while (cur.moveToNext()){
//                    Log.d("Search", "onClick: "+cur.getString(3));
//                }
//            }
//        });

        Cursor cur = mydb.getSenderRecipientMessages(contactNumber,sendercontactNumber);

        while(cur.moveToNext()){
            threadMssging threadMssging = new threadMssging();
            threadMssging.setRecipient_id(cur.getString(1));
            threadMssging.setSender_id(cur.getString(2));
            threadMssging.setRecipient_mssg(cur.getString(3));
            threadMssging.setSender_mssg(cur.getString(4));
            threadMssging.setRecipient_time(cur.getString(5));
            threadMssging.setSender_time(cur.getString(6));
            threadMssgings.add(threadMssging);
        }

        //todo add adapter
        threadmssgingviewadapter threadmssgingviewadapter = new threadmssgingviewadapter(this,threadMssgings);
        mThreadMssging.addItemDecoration(new SimpleDividerItemDecoration(this));
        mThreadMssging.setAdapter(threadmssgingviewadapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //Todo should finish or save the mssg as draft
        finish();
    }
}
