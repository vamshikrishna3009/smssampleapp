package com.example.vamshi.myapplication.broadcast;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsMessage;
import android.widget.Toast;

import com.example.vamshi.myapplication.R;
import com.example.vamshi.myapplication.activity.MainActivity;

import static java.lang.System.currentTimeMillis;

/**
 * Created by vamshi on 25-06-2016.
 */
public class SmsReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle intentExtras = intent.getExtras();
        String strMessage = "";

        if (intentExtras != null) {
            /* Get Messages */
            Object[] sms = (Object[]) intentExtras.get("pdus");
            SmsMessage [] messages = new SmsMessage[sms.length];

            for (int i = 0; i < messages.length; i++)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    String format = intentExtras.getString("format");
                    messages[i] = SmsMessage.createFromPdu((byte[]) sms[i], format);
                }
                else {
                    messages[i] = SmsMessage.createFromPdu((byte[]) sms[i]);
                }
                strMessage += "SMS From: " + messages[i].getOriginatingAddress();
                strMessage += " : ";
                strMessage += messages[i].getMessageBody();
                strMessage += "\n";

                intent = new Intent(context, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                        .setWhen(currentTimeMillis())
                        .setContentTitle(messages[i].getOriginatingAddress())
                        .setContentText(messages[i].getMessageBody())
                        .setAutoCancel(true)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setSound(sound)
                        .setColor(Color.parseColor("#3b90af"))
                        .setPriority(0);

                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);

                builder.setContentIntent(pendingIntent);

                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                Notification notification = builder.build();
                notification.defaults = Notification.DEFAULT_ALL;
                notificationManager.notify(0, notification);

            }

            Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
        }
    }
}